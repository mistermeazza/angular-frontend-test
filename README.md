# TestFe

## Basic Version:
commit: d474a6c0f256065a27af438d2bc6d6e48f995b4a

## Grid Version:
commit: 5d03242039471ab13f1131d6893317574c80f174

## SidebarNav Version:
Branch Master

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.1.2.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.


