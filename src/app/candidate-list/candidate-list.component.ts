import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-candidate-list',
  templateUrl: './candidate-list.component.html',
})
export class CandidateListComponent {

  @Input() candidates=[]
  constructor() {
   }
}
