import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { environment } from 'src/environments/environment';

export class User {
  'results'?:any;
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private httpClient: HttpClient) { 
  }
  
  fetch(){
    const {apiUrl} = environment;
    return this.httpClient.get<User>(apiUrl);
  }
}
