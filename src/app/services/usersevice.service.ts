import { EventEmitter, Output,Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class UsersService {
  @Output() userChanged:EventEmitter<number> = new EventEmitter<number>();
  @Output() userDeleted:EventEmitter<number> = new EventEmitter<number>();
  private selectedUser:number =0;
  constructor() { }

  changeUser(id:number){
    this.userChanged.emit(id);
  }
  deleteUser(id:number){
    this.userDeleted.emit(id);
  }
  getSelectedUser():number{ return this.selectedUser;}
}
