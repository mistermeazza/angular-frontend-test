import {  Component, Injectable, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-profilepic',
  templateUrl: './profilepic.component.html',
})

@Injectable({
  providedIn:'root'
})

export class ProfilepicComponent implements OnInit {
  @Input() imageUrl:string='';
  @Input() loading:boolean=true;
  constructor() { 
  }

  ngOnInit() {
  this.loading = true;
  }

}
