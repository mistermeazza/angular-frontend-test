import { Component, Input, OnChanges,  SimpleChanges } from '@angular/core';
import { UsersService } from '../services/usersevice.service';

@Component({
  selector: 'app-candidate',
  templateUrl: './candidate.component.html'
})

export class CandidateComponent implements OnChanges {
  @Input() candidate:any;
  @Input() id:number = -1;
  avatar:string=''
  name:string=''
  selected:boolean = false;
  location:string=''
  constructor(public userService:UsersService) {
    this.userService.userChanged.subscribe(id=>{
     this.selected = this.id === id;
    })
  }

  ngOnChanges(changes:SimpleChanges):void{
    if(this.candidate){
      this.name = `${this.candidate.name.first} ${this.candidate.name.last}`
      this.avatar = this.candidate.picture.thumbnail
      this.location = this.candidate.location.city;
      this.selected = this.id === this.userService.getSelectedUser();
    }
  }
  view(){
   this.userService.changeUser(this.id);
  }
  remove(){
    this.userService.deleteUser(this.id);
  }





}
