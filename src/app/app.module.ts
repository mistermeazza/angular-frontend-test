import {  HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { ProfilepicComponent } from './profilepic/profilepic.component';
import { ApiService, User } from './services/apiservice.service';
import { ViewerComponent } from './viewer/viewer.component';
import { CandidateListComponent } from './candidate-list/candidate-list.component';
import { CandidateComponent } from './candidate/candidate.component';

import {MatIconModule} from '@angular/material/icon'
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatSidenavModule} from '@angular/material/sidenav';

@NgModule({
  declarations: [
    AppComponent,
    ProfilepicComponent,
    ViewerComponent,
    CandidateListComponent,
    CandidateComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatIconModule,
    MatCardModule,
    MatButtonModule,
    MatSidenavModule
  ],
  providers: [
    ApiService,
    User
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
