import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ProfilepicComponent } from '../profilepic/profilepic.component';
import { ApiService } from '../services/apiservice.service';

const ViewConfg = [
  {
    icon:'person',
    name:'profile',
    intro:'Hi, My name is'
  },
  {
    name:'email',
    icon:'email',
    intro:'My email address is'
  },
  {
    name:'birthday',
    icon:'redeem',
    intro:'My birthday is'
  },
  {
    name:'address',
    icon:'location_on',
    intro:'My birthday is'
  },
  {
    name:'phonenumber',
    icon:'phone',
    intro:'My phone number is'
  },
  {
    name:'loginpwd',
    icon:'fiber_pin',
    intro:'My password is'
  }
];

@Component({
  selector: 'app-viewer',
  templateUrl: './viewer.component.html'
})

export class ViewerComponent implements OnChanges{
  @Input() currentUser:any
  @Input() candidates=[]
  assetPath:string='assets/icons/'
  intro:string='retrieves data from server'
  selected:string = 'profile';
  profileValue:string='...'
  menuItems = ViewConfg
  constructor(
    public thumbNail:ProfilepicComponent,
    private apiService:ApiService
    ) { }

  ngOnChanges(changes: SimpleChanges){
     if(this.currentUser)
        this.showInfo('profile');
  }
  showInfo(section:string){
    this.intro = this.menuItems.find((item)=>{
      return item.name === section;
    })?.intro || ''
    this.getProfileSection(section)
  }
  getProfileSection(section:string){
    this.selected = section;
    switch (section) {
      case 'profile':
        this.profileValue = `${this.currentUser.name.first} ${this.currentUser.name.last}`
        break;
      case 'email':
        this.profileValue = this.currentUser.email;
        break
      case 'birthday':
        this.profileValue = new Date(this.currentUser.dob.date).toLocaleDateString();
        break
      case 'address':
        this.profileValue = `${this.currentUser.location.street.number} ${this.currentUser.location.street.name}`;
        break
      case 'phonenumber':
        this.profileValue = this.currentUser.cell;
        break
      case 'loginpwd':
        this.profileValue = this.currentUser.login.password;
        break
    }
   
  }

}
