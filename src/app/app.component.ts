import { Component, OnInit } from '@angular/core';
import { ApiService } from './services/apiservice.service';
import { UsersService } from './services/usersevice.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
  imageUrl:string=''
  loading:boolean=true;
  currentUser:any;
  profiles = [];
  constructor(
    private apiService:ApiService,
    private userService:UsersService
    ){

  }

  ngOnInit(){
    this.fetchData();
  }

  fetchData() {
    this.apiService.fetch().subscribe(result=>{
      this.profiles = result.results;
      this.loading = false;
      this.updateCurrentProfile(0);
      this.connectServices();
    })
  }

  connectServices(){
    this.userService.userChanged.subscribe(id=>{
      this.updateCurrentProfile(id)
    })
    this.userService.userDeleted.subscribe(id=>{
      this.profiles.splice(id,1);
      this.userService.changeUser(0);
    })
  }
  
  updateCurrentProfile(id:number){
    this.currentUser = this.profiles[id];
    this.imageUrl = this.currentUser.picture.large;
  }
}
